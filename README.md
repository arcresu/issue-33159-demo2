Demo for gitlab-org/gitlab-ce#33159

demo1 is added as a submodule three times: one for each of SHH, HTTPS and local path. Since it's all the same commit, GitLab succeeds in linking all three of them to the correct repo.

demo3 is added as a submodule using only a **local path**. GitLab is unable to create the link for its commit (because how should it know which repo it's referring to if it only has a relative path?), but confusingly it links the name `issue-33159-demo-3` to the current repo.

demo4 is added as a submodule using only **HTTPS**. It links as expected.

demo5 is added as a submodule using only **SSH**.
